const http = require("http");
const app = require("./app");
const host = "localhost";
const port = 80;
const server = http.createServer(app);

server.listen(port, () => {
  console.log("listening to - " + host + ":" + port);
});
