const express = require("express");
const router= express.Router();
const defController= require('./controllers/default.Controller');


router.get('/',defController.home);
router.get('/getcredlist',defController.getDataList);
router.post('/creds',defController.saveCred);
router.put('/creds',defController.updateCred);
router.delete('/creds',defController.deleteCred);

router.post('/sendmail',defController.sendMymail);
module.exports=router;